﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Co_op.Ecommerce.Helpers
{
    public static class TestSettings
    {
        public static string BaseHost
        {
            get { return ConfigurationManager.AppSettings["BaseHost"]; }
        }

        public static string Username
        {
            get { return ConfigurationManager.AppSettings["Username"]; }
        }

        public static string Password
        {
            get { return ConfigurationManager.AppSettings["Password"]; }
        }

        public static string Email
        {
            get { return ConfigurationManager.AppSettings["Email"]; }
        }

        public static string EmailPassword
        {
            get { return ConfigurationManager.AppSettings["EmailPassword"]; }
        }

        public static bool CloseBrowsersAtTheEnd
        {
            get
            {
                return ConfigurationManager.AppSettings["CloseBrowsersAtTheEnd"].Equals("true",
                    StringComparison.InvariantCultureIgnoreCase);
            }
        }

        public static string WebDriverType
        {
            get { return ConfigurationManager.AppSettings["WebDriverType"]; }
        }
    }
}
