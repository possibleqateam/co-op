﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using Co_op.Ecommerce.Helpers;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using System.Drawing;


namespace Co_op.Ecommerce.Core
{
    public class TestStepBase: IDisposable
    {
        protected IWebDriver Driver;

        public TestStepBase()
        {
            switch (TestSettings.WebDriverType) 
            {
                case "IE":
                    Driver = new InternetExplorerDriver();
                    break;
                default:
                       Driver = new ChromeDriver();


                        break;
                    
            }
        }

        public void Dispose()
        {
            if (TestSettings.CloseBrowsersAtTheEnd)
            {
                Driver.Quit();
                Driver.Dispose();
            }
        }
    }
}
