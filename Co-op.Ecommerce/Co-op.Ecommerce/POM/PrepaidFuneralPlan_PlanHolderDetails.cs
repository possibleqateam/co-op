﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;

namespace Co_op.Ecommerce.POM
{
    public class PrepaidFuneralPlan_PlanHolderDetails
    {
        public readonly IWebDriver Driver;

        //[FindsBy(How = How.ClassName, Using = "")]


        //Funeral Plan is for me radio
        [FindsBy(How = How.XPath, Using = "//*[@name='IsThisPlanForPurchaserRadio' and @value='True']")]
        private IWebElement _forMeRadio { get; set; }
        
        //Funeral Plan is for someone else
        [FindsBy(How = How.XPath, Using = "//*[@name='IsThisPlanForPurchaserRadio' and @value='False']")]
        private IWebElement _forSomeoneElseRadio { get; set; }

        //Continue button
        [FindsBy(How = How.Id, Using = "submit-form")]
        private IWebElement _continueButton { get; set; }

        //Save and complete later button
        [FindsBy(How = How.Id, Using = "ssubmit-sacl-form")]
        private IWebElement _saveAndCompleteLaterButton { get; set; }

        public PrepaidFuneralPlan_PlanHolderDetails(IWebDriver webDriver)
        {
            Driver = webDriver;
            PageFactory.InitElements(webDriver, this);
        }

        public static PrepaidFuneralPlan_PlanHolderDetails Navigate(IWebDriver currentWebDriver)
        {
            //currentWebDriver.Navigate().GoToUrl(@"");
            return new PrepaidFuneralPlan_PlanHolderDetails(currentWebDriver);
        }

        public void SelectPlanForMe()
        {
            _forMeRadio.Click();
        }

        public void SelectPlanForSomeoneElse()
        {
            _forSomeoneElseRadio.Click();
        }

        public void ClickOnContinue()
        {
            _continueButton.Click();
        }

        public void ClickOnSaveAndCompleteLater()
        {
            _saveAndCompleteLaterButton.Click();
        }
    }
}
