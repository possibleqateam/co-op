﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Co_op.Ecommerce.Core;
using Co_op.Ecommerce.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System.IO;


namespace Co_op.Ecommerce.POM
{
    public sealed class OnlineMemorial_DeliveryDetails 
    {
        public readonly IWebDriver Driver;
        //Title radio buttons - Mr

        [FindsBy(How = How.XPath, Using = "//*[@name='TitleNameDOB.Title' and @value='Mr']")]
        private IWebElement _mrTitleRadio { get; set; }

        //First name
        [FindsBy(How = How.Id, Using = "TitleNameDOB_FirstName")]
        private IWebElement _firstNameInput { get; set; }

        //Last name
        [FindsBy(How = How.Id, Using = "TitleNameDOB_LastName")]
        private IWebElement _lastNameInput { get; set; }

        //Country
        [FindsBy(How = How.Id, Using = "AddressLookupWithCountry_Country")]
        private IWebElement _countrySelect { get; set; }

        //Postcode
        [FindsBy(How = How.Id, Using = "postcode")]
        private IWebElement _postcodeInput { get; set; }

        //Continue button
        [FindsBy(How = How.Id, Using = "submit-form")]
        private IWebElement _continueButton { get; set; }
        

        public OnlineMemorial_DeliveryDetails(IWebDriver webDriver)
        {
            Driver = webDriver;
            PageFactory.InitElements(webDriver, this);
        }

        public static OnlineMemorial_DeliveryDetails Navigate(IWebDriver currentWebDriver)
        {
            String path = @"Co-op.Ecommerce\Co-op.Ecommerce\Files\PermavitaJourneystestFormQA2.html";
            currentWebDriver.Navigate().GoToUrl(bingPathToAppDir(path));
            return new OnlineMemorial_DeliveryDetails(currentWebDriver);
        }


        public void CheckMrRadioButton()
        {
            _mrTitleRadio.Click();
        }

        public void EnterFirstname()
        {
            _firstNameInput.SendKeys("testingautomation");
        }

        public void EnterLastname()
        {
            _lastNameInput.SendKeys("testingautomation");

        }

        public void SelectCountry()
        {
            var _countryAddress = _countrySelect;

            var selectElementCountry = new SelectElement(_countryAddress);

            selectElementCountry.SelectByText("United Kingdom");

        }


        public void EnterPostcode()
        {
            _postcodeInput.SendKeys("M60 0AG");
            _postcodeInput.SendKeys(Keys.Return);
            Thread.Sleep(5000);

            var _coopAddress = Driver.FindElement(By.XPath(".//*[@class='long address-dropdown']"));

            var selectElement = new SelectElement(_coopAddress);

            // select by text
            selectElement.SelectByText("The Co-operative Group, 1 Angel Square, MANCHESTER M60 0AG");
        }

        public void ContinueToTheNextStep()
        {
            _continueButton.Click();
        }

        public static string bingPathToAppDir(string localPath)
        {
            string currentDir = Environment.CurrentDirectory;
            DirectoryInfo directory = new DirectoryInfo(
            Path.GetFullPath(Path.Combine(currentDir, @"..\..\" + localPath)));
            return directory.ToString();
        }
    }
}
