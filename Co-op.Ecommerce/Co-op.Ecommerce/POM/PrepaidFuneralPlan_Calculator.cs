﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System.Threading;
using OpenQA.Selenium.Interactions;

namespace Co_op.Ecommerce.POM
{
    public class PrepaidFuneralPlan_Calculator
    {
        public readonly IWebDriver Driver;

        //[FindsBy(How = How.ClassName, Using = "")]

        // private WebElement element;

       


        //Choose a type of funeral
        [FindsBy(How = How.Id, Using = "funeral_type")]
        private IWebElement _typeOfFuneralSelect { get; set; }

        //Choose a Funeral Plan
        [FindsBy(How = How.Id, Using = "set_plan_type")]
        private IWebElement _funeralPlanSelect { get; set; }

        //Where would you like to purchase your Funeral Plan?
        //Buy online - radio button 
        [FindsBy(How = How.XPath, Using = "//*[@name='channel' and @value='1']")]
        private IWebElement _buyOnlineRadio { get; set; }

        //Buy at a Funeral home or over the phone - radio button 
        [FindsBy(How = How.XPath, Using = "//*[@name='channel' and @value='2']")]
        private IWebElement _buyAtHomeRadio { get; set; }


        //Choose how you want to pay
        //Pay in full
        [FindsBy(How = How.XPath, Using = "//*[@name='payment_term' and @value='1']")]
        private IWebElement _payInFullRadio { get; set; }

        //for_purchaser
        [FindsBy(How = How.XPath, Using = "//*[@name='for_purchaser' and @value='1']")]
        private IWebElement _payForPurchaserRadio { get; set; }

        //Date of birth
        [FindsBy(How = How.Id, Using = "holder_dob")]
        private IWebElement _calendarInput { get; set; }

        //Pay by instalments
        [FindsBy(How = How.XPath, Using = "//*[@name='payment_term' and @value='2']")]
        private IWebElement _payByInstalmentsRadio { get; set; }

        //Are you a Co-op member?
        [FindsBy(How = How.XPath, Using = "//*[@name='have_membership' and @value='1']")]
        private IWebElement _memberYesRadio { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@name='have_membership' and @value='2']")]
        private IWebElement _memberNoRadio { get; set; }


        //Do you have a promotional code?  
        [FindsBy(How = How.XPath, Using = "//*[@name='have_promotion_code' and @value='1']")]
        private IWebElement _promoYesRadio { get; set; }

        [FindsBy(How = How.XPath, Using = "//*[@name='have_promotion_code' and @value='2']")]
        private IWebElement _promoNoRadio { get; set; }


        //Calculate
        [FindsBy(How = How.XPath, Using = "//*[@type='submit' and @value='Calculate']")]
        private IWebElement _calculateButton { get; set; }

        //I'm ready to continue button
        [FindsBy(How = How.Id, Using = "//*[@class='btn btn-primary has-icon right-icon' and @title='Purchase a Set Funeral Plan']")]  
        private IWebElement _readytocontinueButton { get; set; }

        

        public PrepaidFuneralPlan_Calculator(IWebDriver webDriver)
        {
            Driver = webDriver;
          
            PageFactory.InitElements(webDriver, this);
        }

        public static PrepaidFuneralPlan_Calculator Navigate(IWebDriver currentWebDriver)
        {
            currentWebDriver.Navigate().GoToUrl(@"https://qa2-coop-fnc.possiblepoland.pl/pre-paid-funeral-plans/our-funeral-plans/funeral-plan-calculator/");
            return new PrepaidFuneralPlan_Calculator(currentWebDriver);
        }
       
        public void SelectTypeOfFuneralBurial()
        {
            var typeSelect = _typeOfFuneralSelect;
            var selectElementFuneralType = new SelectElement(typeSelect);
            selectElementFuneralType.SelectByValue("2");
        }

        public void SelectTypeOfFuneralCremation()
        {
            var typeSelect = _typeOfFuneralSelect;
            var selectElementFuneralType = new SelectElement(typeSelect);
            selectElementFuneralType.SelectByValue("1");
        }

        public void SelectFuneralPlanSimple()
        {
            var planSelect = _funeralPlanSelect;
            var selectElementFuneralPlan = new SelectElement(planSelect);
            selectElementFuneralPlan.SelectByValue("1");
        }

        public void SelectFuneralPlanBronze()
        {
            var planSelect = _funeralPlanSelect;
            var selectElementFuneralPlan = new SelectElement(planSelect);
            selectElementFuneralPlan.SelectByValue("2");
        }

        public void SelectFuneralPlanSilver()
        {
            var planSelect = _funeralPlanSelect;
            var selectElementFuneralPlan = new SelectElement(planSelect);
            selectElementFuneralPlan.SelectByValue("3");
        }

        public void SelectFuneralPlanGold()
        {
            var planSelect = _funeralPlanSelect;
            var selectElementFuneralPlan = new SelectElement(planSelect);
            selectElementFuneralPlan.SelectByValue("4");
        }

        public void BuyFuneralPlanOnline()
        {
            _buyOnlineRadio.Click();
        }

        public void BuyFuneralPlanAtHome()
        {
            _buyAtHomeRadio.Click();
        }

        public void PayInFull()
        {
            _payInFullRadio.Click();

            _payForPurchaserRadio.Click();

            //DOB
            IJavaScriptExecutor js = Driver as IJavaScriptExecutor;

            Actions actions = new Actions(Driver);
            actions.MoveToElement(_calendarInput);
            actions.Perform();

            actions.ClickAndHold(_calendarInput);
            Thread.Sleep(1000);
            actions.Perform();
            Thread.Sleep(1000);
            Driver.FindElement(By.XPath(".//*[@class='pika-lendar']/div/div/select/option[3]")).Click();
            Driver.FindElement(By.XPath(".//*[@class='pika-lendar']/div/div[2]/select/option[85]")).Click();

            Driver.FindElement(By.XPath(".//*[@class='pika-lendar']/table/tbody/tr[5]/td[4]/button")).Click();

            
               
        }

        public void PayByInstalements()
        {
            _payByInstalmentsRadio.Click();
        }

        public void NoCoopMember()
        {
            Thread.Sleep(1000);
            _memberNoRadio.Click();
        }

        public void PromoCode()
        {
            _promoNoRadio.Click();
        }

        public void ClickOnCalculate()
        {
            _calculateButton.Click();
        }

        public void ClickOnPay()
        {
            _readytocontinueButton.Click();
        }

        private bool IsElementPresent(By by)
         {
             try
             {
                 Driver.FindElement(by);
                 return true;
             }
             catch (NoSuchElementException)
             {
                 return false;
             }
         }

      

    }
}
