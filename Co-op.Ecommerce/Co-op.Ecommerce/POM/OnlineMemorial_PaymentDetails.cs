﻿using System;
using System.Text;
using TechTalk.SpecFlow;
using Co_op.Ecommerce.Core;
using Co_op.Ecommerce.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System.IO;

namespace Co_op.Ecommerce.POM
{
    public sealed class OnlineMemorial_PaymentDetails
    {
       public readonly IWebDriver Driver;

       /////////////////////// CARD DETAILS
       //Type of card
       [FindsBy(How = How.Id, Using = "cardType")]
       private IWebElement _typeOfCardSelect { get; set; }

       //Name on Card
       [FindsBy(How = How.Id, Using = "nameOnCard")]
       private IWebElement _nameOnCardInput { get; set; }

      

       //Expire date - Month
       [FindsBy(How = How.Id, Using = "gatewayCardExpiryDateMonth")]
       private IWebElement _expiryMonthSelect { get; set; }

       //Expire date - Year
       [FindsBy(How = How.Id, Using = "gatewayCardExpiryDateYear")]
       private IWebElement _expiryYearSelect { get; set; }

       //_postcodeInput 
       [FindsBy(How = How.Id, Using = "postcode")]
       private IWebElement _postcodeInput { get; set; }

       //Correct address - Yes Radio
       [FindsBy(How = How.XPath, Using = "//*[@name='BillingAddress.IsThisCorrectAddress' and @value='True']")]
       private IWebElement _addressConfirmYesRadio { get; set; }

       //Correct address - No Radio
       [FindsBy(How = How.XPath, Using = "//*[@name='BillingAddress.IsThisCorrectAddress' and @value='False']")]
       private IWebElement _addressConfirmNoRadio { get; set; }

        //Membership

       //Membership - Yes Radio
       [FindsBy(How = How.XPath, Using = "//*[@name='CoopMembership.IsMembershipNumber' and @value='True']")]
       private IWebElement _membershipConfirmYesRadio { get; set; }

       //Membership - No Radio
       [FindsBy(How = How.XPath, Using = "//*[@name='CoopMembership.IsMembershipNumber' and @value='False']")]
       private IWebElement _membershipConfirmNoRadio { get; set; }


        //Terms & Conditions
       //T&C - Yes Radio
       [FindsBy(How = How.XPath, Using = "//*[@name='TermCondition.IsTermAccepted' and @value='True']")]
       private IWebElement _termsConfirmYesRadio { get; set; }

       //T&C - No Radio
       [FindsBy(How = How.XPath, Using = "//*[@name='TermCondition.IsTermAccepted' and @value='False']")]
       private IWebElement _termsConfirmNoRadio { get; set; }


       //Continue button
       [FindsBy(How = How.Id, Using = "submit-form")]
       private IWebElement _continueButton { get; set; }

       public OnlineMemorial_PaymentDetails(IWebDriver webDriver)
        {
            Driver = webDriver;
            PageFactory.InitElements(webDriver, this);
        }

       public static OnlineMemorial_PaymentDetails Navigate(IWebDriver currentWebDriver)
        {
            
            return new OnlineMemorial_PaymentDetails(currentWebDriver);
        }

       public void SelectCardType()
       {
           var cardSelect = _typeOfCardSelect;
           var selectElementCardType = new SelectElement(cardSelect);
           selectElementCardType.SelectByValue("mastercard");
       }

       public void EnterNameOnCard()
       {
           _nameOnCardInput.SendKeys("testingautomation");
       }


       public void SelectExpiryDate()
       {
           var monthSelect = _expiryMonthSelect;
           var selectElementMonth = new SelectElement(monthSelect);
           selectElementMonth.SelectByValue("5");

           var yearSelect = _expiryYearSelect;
           var selectElementYear = new SelectElement(yearSelect);
           selectElementYear.SelectByValue("2021");

       }

      
       public void EnterPostcode()
       {
           _postcodeInput.SendKeys("M60 0AG");
           _postcodeInput.SendKeys(Keys.Return);
           Thread.Sleep(5000);

           var _coopAddress = Driver.FindElement(By.XPath(".//*[@class='long address-dropdown']"));

           var selectElement = new SelectElement(_coopAddress);

           // select by text
           selectElement.SelectByText("The Co-operative Group, 1 Angel Square, MANCHESTER M60 0AG");
       }

       public void EnterCardNumber()
       {
           var _frameCardNumber = Driver.FindElement(By.ClassName("gw-proxy-number"));
           Driver.SwitchTo().Frame(_frameCardNumber);
           Driver.FindElement(By.CssSelector("input")).SendKeys("2223000000000023");

       }

       public void EnterCVCCode()
       {
           Driver.SwitchTo().ParentFrame();
           var _frameCVCNumber = Driver.FindElement(By.ClassName("gw-proxy-securityCode"));
           Driver.SwitchTo().Frame(_frameCVCNumber);
           Driver.FindElement(By.CssSelector("input")).SendKeys("222");
           Driver.SwitchTo().ParentFrame();
       }

       public void SelectBillingAddress()
       {
          
           if(IsElementPresent(By.XPath("//*[@name='BillingAddress.IsThisCorrectAddress' and @value='True']")))
            {
                _addressConfirmYesRadio.Click();
            }
            else
            {
                EnterPostcode();
            }
       }

       public void SelectMembership()
       {
           _membershipConfirmNoRadio.Click();
       }

       public void AcceptTerms()
       {
           _termsConfirmYesRadio.Click();
       }

       public void ClickOnContinue()
       {
         
           _continueButton.Click();
       }

       private bool IsElementPresent(By by)
       {
           try
           {
               Driver.FindElement(by);
               return true;
           }
           catch (NoSuchElementException)
           {
               return false;
           }
       }
    }
}
