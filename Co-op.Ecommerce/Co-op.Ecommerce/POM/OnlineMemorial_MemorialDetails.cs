﻿using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Co_op.Ecommerce.POM
{
    public class OnlineMemorial_MemorialDetails
    {
          public readonly IWebDriver Driver;

        //[FindsBy(How = How.ClassName, Using = "")]

        // private WebElement element;

          public OnlineMemorial_MemorialDetails(IWebDriver webDriver)
        {
            Driver = webDriver;
            PageFactory.InitElements(webDriver, this);
        }

        public static OnlineMemorial_MemorialDetails Navigate(IWebDriver currentWebDriver)
        {
            return new OnlineMemorial_MemorialDetails(currentWebDriver);
            
        }

    }
}
