﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support;
using OpenQA.Selenium.Interactions;
using System.Threading;
using NUnit.Framework;

namespace Co_op.Ecommerce.POM
{
    public class Flowers_YourDetails
    {
        public readonly IWebDriver Driver;
        private string randomEmail;

        //Header H1
        [FindsBy(How = How.TagName, Using = "H1")]
        private IWebElement _h1TitleText { get; set; }

        //Title radio buttons - Mr

        [FindsBy(How = How.XPath, Using = "//*[@name='TitleNameDOB.Title' and @value='Mr']")]
        private IWebElement _mrTitleRadio { get; set; }

        //Other title radio buttons ...



        //First name
        [FindsBy(How = How.Id, Using = "TitleNameDOB_FirstName")]
        private IWebElement _firstNameInput { get; set; }

        //Last name
        [FindsBy(How = How.Id, Using = "TitleNameDOB_LastName")]
        private IWebElement _lastNameInput { get; set; }

        //Date of birth
        [FindsBy(How = How.Id, Using = "TitleNameDOB_DateOfBirth")]
        private IWebElement _calendarInput { get; set; }


        //Telephone number
        [FindsBy(How = How.Id, Using = "PhoneEmail_TelephoneNumber")]
        private IWebElement _telephoneInput { get; set; }

        //Email
        [FindsBy(How = How.Id, Using = "PhoneEmail_Email")]
        private IWebElement _emailInput { get; set; }

        //Confirm Email
        [FindsBy(How = How.Id, Using = "email-confirm")]
        private IWebElement _confirmEmailInput { get; set; }

        //Continue button
        [FindsBy(How = How.Id, Using = "submit-form")]
        private IWebElement _continueButton { get; set; }

        //First tab
        [FindsBy(How = How.ClassName, Using = "steps__item  steps__item--first steps__item--active")]
        private IWebElement _firstTab { get; set; }


        public Flowers_YourDetails(IWebDriver webDriver)
        {
            Driver = webDriver;
            PageFactory.InitElements(webDriver, this);
        }

       

        public static Flowers_YourDetails Navigate(IWebDriver currentWebDriver)
        {
            currentWebDriver.Navigate().GoToUrl(@"http://qa2-coop-fnc.possiblepoland.pl/arranging-a-funeral/organising-the-day/floral-tributes/");
            //currentWebDriver.Manage().Window.Maximize();

            Thread.Sleep(15000);
            //currentWebDriver.Manage().Timeouts().ImplicitWait (TimeSpan.FromSeconds(15));

           


            IWebElement item = currentWebDriver.FindElement(By.PartialLinkText("Find out more"));
            item.Click();

            Thread.Sleep(4000);
            //currentWebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(4));
           
            IWebElement postcode = currentWebDriver.FindElement(By.Id("postcode_funeral"));
            postcode.SendKeys("HU93DQ");
            postcode.SendKeys(Keys.Return);
            Thread.Sleep(10000);
           // IWebElement postcodeMagnifyingGlass = currentWebDriver.FindElement(By.Id("store-locate-by-address-flowers"));

            IWebElement addressFirstRadio = currentWebDriver.FindElement(By.Id("fh_0-address-postcode"));
            addressFirstRadio.Click();

            Thread.Sleep(3000);
            //currentWebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(3));

            //The name of the deceased
            IWebElement nameOfDeceased = currentWebDriver.FindElement(By.Id("recipients_name"));
            nameOfDeceased.SendKeys("test");

            //Send a message with your flowers
            IWebElement message = currentWebDriver.FindElement(By.Id("message"));
            message.SendKeys("test");

            IWebElement calendar = currentWebDriver.FindElement(By.Id("service_date"));
            
           
            IJavaScriptExecutor js = currentWebDriver as IJavaScriptExecutor;
            
            Actions actions = new Actions(currentWebDriver);
            actions.MoveToElement(calendar);
            actions.Perform();

            SelectElement hours = new SelectElement(currentWebDriver.FindElement(By.Id("service_time_hour")));

            var hourSelect = currentWebDriver.FindElement(By.Id("service_time_hour"));
            var selectElementHour = new SelectElement(hourSelect);
           
            selectElementHour.SelectByValue("12");

            var minuteSelect = currentWebDriver.FindElement(By.Id("service_time_minutes"));
            var selectElementMinute = new SelectElement(minuteSelect);
            selectElementMinute.SelectByValue("30");

           // actions.ClickAndHold(calendar);
         //   actions.Perform();
            //js.ExecuteScript("document.querySelector('#service_date').click();");

            
              //  js.ExecuteScript("document.querySelector('#service_date').click();");
             //   js.ExecuteScript("document.querySelector('#service_date').value= '';");
            actions.ClickAndHold(calendar);
            Thread.Sleep(10000);
            actions.Perform();
            Thread.Sleep(10000);
                //js.ExecuteScript("document.querySelector('#service_date').value= '31/12/17';");
               js.ExecuteScript("document.querySelector('[data-pika-day=\"28\"]').click();");
                
           
            
          
             
            //calendar.SendKeys(OpenQA.Selenium.Keys.Tab);
           // js.ExecuteScript("document.querySelector('#service_date').value= '31/12/17';");
            
            //js.ExecuteScript("document.querySelector('#service_date').focus();");
           // currentWebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(5));
          //  js.ExecuteScript("document.querySelector('#service_date').click();");
         
           //   js.ExecuteScript("document.querySelector('[data-pika-day=\"30\"]').click();");
            //   Thread.Sleep(4000);
          
           // js.ExecuteScript("document.querySelector('#service_date').value= '31/12/17';");


            //Submit
            IWebElement submit = currentWebDriver.FindElement(By.Name("submit"));
            submit.Click();

            Thread.Sleep(8000);
            //currentWebDriver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(8));
            
            return new Flowers_YourDetails(currentWebDriver);
        }

        public void CheckShoppingBasket()
        {
           
            IWebElement title = Driver.FindElement(By.TagName("H3"));
            
            //Assert.True(title.Text.Contains("You have [1] items in your basket…"));


            Assert.True(Driver.Title.Contains("Basket"));
            IWebElement submit = Driver.FindElement(By.Id("submit"));
            submit.Click();
            Thread.Sleep(5000);

        }


        public void CheckMrRadioButton()
        {
            _mrTitleRadio.Click();
        }

        public void EnterFirstname()
        {
            _firstNameInput.SendKeys("testingautomation");
        }

        public void EnterLastname()
        {
            _lastNameInput.SendKeys("testingautomation");

        }

        public void EnterDOB()
        {
            IJavaScriptExecutor js = Driver as IJavaScriptExecutor;

            Actions actions = new Actions(Driver);
            actions.MoveToElement(_calendarInput);
            actions.Perform();

            actions.ClickAndHold(_calendarInput);
            Thread.Sleep(1000);
            actions.Perform();
            Thread.Sleep(1000);
            Driver.FindElement(By.XPath(".//*[@class='pika-lendar']/div/div/select/option[3]")).Click();
            Driver.FindElement(By.XPath(".//*[@class='pika-lendar']/div/div[2]/select/option[85]")).Click();

            Driver.FindElement(By.XPath(".//*[@class='pika-lendar']/table/tbody/tr[5]/td[4]/button")).Click();

            //js.ExecuteScript("document.querySelector('#service_date').value= '31/12/17';");


            //js.ExecuteScript("document.querySelector('[pika-select pika-select-month=\"2\"]').click();");


            //js.ExecuteScript("document.querySelector('[pika-select pika-select-year=\"1983\"]').click();");

            //document.querySelector("div.pika-lendar select[class='pika-select pika-select-month']");

        }


        public void EnterTelephoneNumber()
        {
            _telephoneInput.SendKeys("7700900736");

        }

        public void EnterEmailAddress()
        {
            Random randomGenerator = new Random();
            int randomInt = randomGenerator.Next(10000);
            randomEmail = "testingautomation" + randomInt + "@possible.com";
            _emailInput.SendKeys(randomEmail);
            _confirmEmailInput.SendKeys(randomEmail);
        }

        public void ContinueToTheNextStep()
        {
            
            _continueButton.Click();
            Thread.Sleep(1000);
            _continueButton.Click();
        }

        public void CheckStepsPanel()
        {
            //Check if panel with steps is visible 
        }

        public void CheckIfCorrectStep()
        {
            //Check if current Step = "1 Your Details"
            //_firstTab.
        }
    }
}
