﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;

namespace Co_op.Ecommerce.POM
{
    public class PrepaidFuneralPlan_ThankYou
    {
           public readonly IWebDriver Driver;

           //Do you agree - Yes radio
           [FindsBy(How = How.XPath, Using = "//*[@name='TermConditions.IsTermAccepted' and @value='True']")]
           private IWebElement _agreeYesRadio { get; set; }

           //Do you agree - No radio
           [FindsBy(How = How.XPath, Using = "//*[@name='TermConditions.IsTermAccepted' and @value='False']")]
           private IWebElement _agreeNoRadio { get; set; }

           //Pay now button
           [FindsBy(How = How.Id, Using = "submit-form")]
           private IWebElement _payNowButton { get; set; }

           public PrepaidFuneralPlan_ThankYou(IWebDriver webDriver)
        {
            Driver = webDriver;
            PageFactory.InitElements(webDriver, this);
        }

           public static PrepaidFuneralPlan_ThankYou Navigate(IWebDriver currentWebDriver)
        {
            
            return new PrepaidFuneralPlan_ThankYou(currentWebDriver);
        }

           public void Iagree()
           {
               _agreeYesRadio.Click();
           }

           public void Disagree()
           {
               _agreeNoRadio.Click();
           }

           public void ClickOnContinue()
           {
               _payNowButton.Click();
           }
    }
}
