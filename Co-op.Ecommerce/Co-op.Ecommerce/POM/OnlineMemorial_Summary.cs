﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;

namespace Co_op.Ecommerce.POM
{
    public class OnlineMemorial_Summary
    {
         public readonly IWebDriver Driver;

        //[FindsBy(How = How.ClassName, Using = "")]

        // private WebElement element;

         public OnlineMemorial_Summary(IWebDriver webDriver)
        {
            Driver = webDriver;
            PageFactory.InitElements(webDriver, this);
        }

         public static OnlineMemorial_Summary Navigate(IWebDriver currentWebDriver)
        {
            currentWebDriver.Navigate().GoToUrl(@"http://qa3.coop-fnc.possiblepoland.pl");
            return new OnlineMemorial_Summary(currentWebDriver);
        }
    }
}
