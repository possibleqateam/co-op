﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System.Threading;
using OpenQA.Selenium.Support.UI;

namespace Co_op.Ecommerce.POM
{
    public class PrepaidFuneralPlan_YourDetails
    {
         public readonly IWebDriver Driver;
        private string randomEmail;

        //[FindsBy(How = How.ClassName, Using = "")]

         //Header H1
         [FindsBy(How = How.TagName, Using = "H1")]
         private IWebElement _h1TitleText { get; set; }

        //Title radio buttons - Mr

         [FindsBy(How = How.XPath, Using = "//*[@name='TitleNameDOB.Title' and @value='Mr']")]
         private IWebElement _mrTitleRadio { get; set; }

        //Other title radio buttons ...



        //First name
         [FindsBy(How = How.Id, Using = "TitleNameDOB_FirstName")]
         private IWebElement _firstNameInput { get; set; }

         //Last name
         [FindsBy(How = How.Id, Using = "TitleNameDOB_LastName")]
         private IWebElement _lastNameInput { get; set; }

        //Date of birth
         [FindsBy(How = How.Id, Using = "TitleNameDOB_DateOfBirth")]
         private IWebElement _calendarInput { get; set; }

        //Postcode
         [FindsBy(How = How.Id, Using = "postcode")]
         private IWebElement _postcodeInput { get; set; }

        //Tel number
         [FindsBy(How = How.Id, Using = "PhoneEmail_TelephoneNumber")]
         private IWebElement _telephoneInput { get; set; }

         //Email
         [FindsBy(How = How.Id, Using = "PhoneEmail_Email")]
         private IWebElement _emailInput { get; set; }

         //Confirm Email
         [FindsBy(How = How.Id, Using = "email-confirm")]
         private IWebElement _confirmEmailInput { get; set; }

        //Membership radio - Yes
        
         [FindsBy(How = How.XPath, Using = "//*[@name='Membership.IsMembershipNumber' and @value='True']")]
         private IWebElement _membershipYesRadio { get; set; }

         //Membership radio - No
         [FindsBy(How = How.XPath, Using = "//*[@name='Membership.IsMembershipNumber' and @value='False']")]
         private IWebElement _membershipNoRadio { get; set; }

        //Password
         [FindsBy(How = How.Id, Using = "PasswordSetUp_Password")]
         private IWebElement _passwordInput { get; set; }

         //Confirm Password
         [FindsBy(How = How.Id, Using = "PasswordSetUp_PasswordConfirmation")]
         private IWebElement _confirmPasswordInput { get; set; }

        //Continue button
         [FindsBy(How = How.Id, Using = "submit-form")]
         private IWebElement _continueButton { get; set; }


         public PrepaidFuneralPlan_YourDetails(IWebDriver webDriver)
        {
            Driver = webDriver;
            PageFactory.InitElements(webDriver, this);
        }

         public static PrepaidFuneralPlan_YourDetails Navigate(IWebDriver currentWebDriver)
        {
            //currentWebDriver.Navigate().GoToUrl(@"http://qa2-coop-fnc.possiblepoland.pl/ecommercce/pre-paid-funeral-plans-journey2/step-1-your-details/Unauthenticated");
            return new PrepaidFuneralPlan_YourDetails(currentWebDriver);
        }


        public void CheckMrRadioButton()
         {
             _mrTitleRadio.Click();
         }

        public void EnterFirstname()
        {
            _firstNameInput.SendKeys("testingautomation");
        }

        public void EnterLastname()
        {
            _lastNameInput.SendKeys("testingautomation");

        }

        public void EnterDOB()
        {
            IJavaScriptExecutor js = Driver as IJavaScriptExecutor;

            Actions actions = new Actions(Driver);
            actions.MoveToElement(_calendarInput);
            actions.Perform();

            actions.ClickAndHold(_calendarInput);
            Thread.Sleep(1000);
            actions.Perform();
            Thread.Sleep(1000);
            Driver.FindElement(By.XPath(".//*[@class='pika-lendar']/div/div/select/option[3]")).Click();
            Driver.FindElement(By.XPath(".//*[@class='pika-lendar']/div/div[2]/select/option[85]")).Click();

            Driver.FindElement(By.XPath(".//*[@class='pika-lendar']/table/tbody/tr[5]/td[4]/button")).Click();

            //js.ExecuteScript("document.querySelector('#service_date').value= '31/12/17';");
            
            
            //js.ExecuteScript("document.querySelector('[pika-select pika-select-month=\"2\"]').click();");
            
            
            //js.ExecuteScript("document.querySelector('[pika-select pika-select-year=\"1983\"]').click();");

            //document.querySelector("div.pika-lendar select[class='pika-select pika-select-month']");
               
        }

        public void EnterPostcode()
        {
            _postcodeInput.SendKeys("M60 0AG");
            _postcodeInput.SendKeys(Keys.Return);
            Thread.Sleep(5000);

            var _coopAddress = Driver.FindElement(By.XPath(".//*[@class='long address-dropdown']"));

            var selectElement = new SelectElement(_coopAddress);

            // select by text
            selectElement.SelectByText("The Co-operative Group, 1 Angel Square, MANCHESTER M60 0AG");
        }

        public void EnterTelephoneNumber()
        {
            _telephoneInput.SendKeys("7700900736");
           
        }

        public void EnterEmailAddress()
        {
            Random randomGenerator = new Random();
            int randomInt = randomGenerator.Next(10000);
            randomEmail = "testingautomation" + randomInt + "@possible.com";
            _emailInput.SendKeys(randomEmail);
            _confirmEmailInput.SendKeys(randomEmail);
        }
        
        public void SelectMembership()
        {
            _membershipNoRadio.Click();
        }

        public void EnterPassword()
        {
            _passwordInput.SendKeys("12345678P");
        }

        public void ConfirmPassword()
        {
            _confirmPasswordInput.SendKeys("12345678P");
        }

        public void ContinueToTheNextStep()
        {
            _continueButton.Click();
        }

    }
}
