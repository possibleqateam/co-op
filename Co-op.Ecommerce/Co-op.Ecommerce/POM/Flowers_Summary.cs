﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace Co_op.Ecommerce.POM
{
    public sealed class Flowers_Summary
    {
        public readonly IWebDriver Driver;

        //[FindsBy(How = How.ClassName, Using = "")]

        // private WebElement element;

        //Your Order first block
        [FindsBy(How = How.ClassName, Using = "block  block--generic  border")]
        private IWebElement _yourOrderSection { get; set; }

        //Your Order - Edit order details
        [FindsBy(How = How.PartialLinkText, Using = "Edit your details")]
        private IWebElement _editOrderDetails { get; set; }


        //Your details - second block
        [FindsBy(How = How.ClassName, Using = "block  block--generic  border")]
        private IWebElement _yourDetailsSection { get; set; }

        //Your details - Edit your details
        [FindsBy(How = How.PartialLinkText, Using = "Edit your details")]
        private IWebElement _editYourDetails { get; set; }


        //Payment information - third block
        [FindsBy(How = How.ClassName, Using = "block  block--generic  border")]
        private IWebElement _paymentInformation { get; set; }

        //Payment information - Edit card details
        [FindsBy(How = How.PartialLinkText, Using = "Edit card details")]
        private IWebElement _editCardDetails { get; set; }

        //Breakdown of costs
        [FindsBy(How = How.ClassName, Using = "breakdown-of-costs  h-m")]
        private IWebElement _totalOrder { get; set; }

        //Terms & Conditions - radio button Yes
        [FindsBy(How = How.XPath, Using = "//*[@name='TermConditions.IsTermAccepted' and @value='True']")]
        private IWebElement _yesTermsRadio { get; set; }


        //Terms & Conditions - radio button No
          [FindsBy(How = How.XPath, Using = "//*[@name='TermConditions.IsTermAccepted' and @value='False']")]
        private IWebElement _noTermsRadio { get; set; }

        //Pay now button
        [FindsBy(How = How.Id, Using = "submit-form")]
        private IWebElement _payButton { get; set; }


        public Flowers_Summary(IWebDriver webDriver)
        {
            Driver = webDriver;
            PageFactory.InitElements(webDriver, this);
            
        }

        public static Flowers_Summary Navigate(IWebDriver currentWebDriver)
        {
           
            return new Flowers_Summary(currentWebDriver);
        }

        public void AcceptTerms()
        {
            _yesTermsRadio.Click();
        }

        public void ClickPay()
        {
            _payButton.Click();
        }
    }
}
