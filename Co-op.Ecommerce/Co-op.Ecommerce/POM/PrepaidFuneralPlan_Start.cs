﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace Co_op.Ecommerce.POM
{
    public class PrepaidFuneralPlan_Start
    {
        public readonly IWebDriver Driver;

        //[FindsBy(How = How.ClassName, Using = "")]

        // private WebElement element;

        //Cremation - radio button 
        [FindsBy(How = How.XPath, Using = "//*[@name='FuneralType' and @value='Cremation']")]
        private IWebElement _cremationRadio { get; set; }


        //Burial - radio button 
        [FindsBy(How = How.XPath, Using = "//*[@name='FuneralType' and @value='Burial']")]
        private IWebElement _burialRadio { get; set; }

        //Plan Level - Simple - radio button 
        [FindsBy(How = How.XPath, Using = "//*[@name='PlanType' and @value='Simple']")]
        private IWebElement _simplePlanRadio { get; set; }

        //Plan Level - Bronze - radio button 
        [FindsBy(How = How.XPath, Using = "//*[@name='PlanType' and @value='Bronze']")]
        private IWebElement _bronzePlanRadio { get; set; }

        //Plan Level - Silver - radio button 
        [FindsBy(How = How.XPath, Using = "//*[@name='PlanType' and @value='Silver']")]
        private IWebElement _silverPlanRadio { get; set; }

        //Plan Level - Gold - radio button 
        [FindsBy(How = How.XPath, Using = "//*[@name='PlanType' and @value='Gold']")]
        private IWebElement _goldPlanRadio { get; set; }


        //I'm ready to continue button
        [FindsBy(How = How.Id, Using = "submit")]
        private IWebElement _readytocontinueButton { get; set; }

        //Error message
        [FindsBy(How = How.LinkText, Using = "Please choose the type of funeral you wish to purchase")]
        private IWebElement _errorMessageForFuneralType { get; set; }

        public PrepaidFuneralPlan_Start(IWebDriver webDriver)
        {
            Driver = webDriver;
            PageFactory.InitElements(webDriver, this);
        }

        public static PrepaidFuneralPlan_Start Navigate(IWebDriver currentWebDriver)
        {
            currentWebDriver.Navigate().GoToUrl(@"https://qa2-coop-fnc.possiblepoland.pl/ecommerce/pre-paid-funeral-plans/select-funeral-plan/");
            return new PrepaidFuneralPlan_Start(currentWebDriver);
        }

        public void ClickImReadytoContinue()
        {
            _readytocontinueButton.Click();
        }

         public void SelectCremationRadio()
        {
            _cremationRadio.Click();
        }

         public void SelectBurialRadio()
        {
            _burialRadio.Click();
        }

         public void SelectSimplePlanRadio()
         {
             _simplePlanRadio.Click();
         }

         public void SelectBronzePlanRadio()
         {
             _bronzePlanRadio.Click();
         }

         public void SelectSilverPlanRadio()
         {
             _silverPlanRadio.Click();
         }

         public void SelectGoldPlanRadio()
         {
             _goldPlanRadio.Click();
         }

         private bool IsElementPresent(By by)
         {
             try
             {
                 Driver.FindElement(by);
                 return true;
             }
             catch (NoSuchElementException)
             {
                 return false;
             }
         }

        public void CheckCremationRadioIfExist()
        {
            if (IsElementPresent(By.XPath("//*[@id='FuneralType' and @value='Cremation']")))
            {
                //do if exists
            }
            else
            {
                //do if does not exists
            }
        }

        public void CheckBurialRadioIfExist()
        {
            if (IsElementPresent(By.XPath("//*[@id='FuneralType' and @value='Burial']")))
            {
                //do if exists
            }
            else
            {
                //do if does not exists
            }
        }

        public void CheckErrorMessage()
        {
            if (IsElementPresent(By.LinkText("Please choose the type of funeral you wish to purchase")))
            {
                //do if exists
            }
            else
            {
                //do if does not exists
            }
        }

    }
}
