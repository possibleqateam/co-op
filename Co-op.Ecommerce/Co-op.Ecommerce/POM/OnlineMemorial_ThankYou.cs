﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace Co_op.Ecommerce.POM
{
    public class OnlineMemorial_ThankYou
    {
         public readonly IWebDriver Driver;

        //[FindsBy(How = How.ClassName, Using = "")]

        // private WebElement element;

         public OnlineMemorial_ThankYou(IWebDriver webDriver)
        {
            Driver = webDriver;
            PageFactory.InitElements(webDriver, this);
        }

         public static OnlineMemorial_ThankYou Navigate(IWebDriver currentWebDriver)
        {
            currentWebDriver.Navigate().GoToUrl(@"http://qa3.coop-fnc.possiblepoland.pl");
            return new OnlineMemorial_ThankYou(currentWebDriver);
        }
    }
}
