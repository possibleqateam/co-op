﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System.Threading;

namespace Co_op.Ecommerce.POM
{
    public class PrepaidFuneralPlan_PaymentDetails
    {
        public readonly IWebDriver Driver;

        //[FindsBy(How = How.ClassName, Using = "")]

        //Full payment radio
        [FindsBy(How = How.XPath, Using = "//*[@name='IsPayingInFull' and @value='True']")]
        private IWebElement _fullPaymentRadio { get; set; }

        //Direct Debit radio
        [FindsBy(How = How.XPath, Using = "//*[@name='IsPayingInFull' and @value='False']")]
        private IWebElement _directDebitRadio { get; set; }


        //Promo code Yes - radio
        [FindsBy(How = How.XPath, Using = "//*[@name='Promocode.IsPromotionalCode' and @value='True']")]
        private IWebElement _promoCodeYesRadio { get; set; }

        //Promo code No - radio
       [FindsBy(How = How.XPath, Using = "//*[@name='Promocode.IsPromotionalCode' and @value='False']")]
        private IWebElement _promoCodeNoRadio { get; set; }

        //Promo code - input
        [FindsBy(How = How.Id, Using = "Promocode_PromotionalCodeField")]
        private IWebElement _promoCodeInput { get; set; }

        /////////////////////// CARD DETAILS
        //Type of card
        [FindsBy(How = How.Id, Using = "cardType")]
        private IWebElement _typeOfCardSelect { get; set; }

        //Name on Card
        [FindsBy(How = How.Id, Using = "nameOnCard")]
        private IWebElement _nameOnCardInput { get; set; }

        //Card number ??????????????
        [FindsBy(How = How.XPath, Using = "//input[@type='tel' and @style='animation: none 0s ease 0s 1 normal none running; background: none 0% 0% / auto repeat scroll padding-box border-box rgb(255, 255, 255); background-blend-mode: normal; border: 1px solid rgb(110, 110, 110); border-radius: 0px; border-collapse: separate; bottom: auto; box-shadow: none; box-sizing: border-box; break-after: auto; break-before: auto; break-inside: auto; caption-side: top; clear: none; clip: auto; color: rgb(40, 40, 40); cursor: text; direction: ltr; display: block; empty-cells: show; float: none; font-family: AvenirNext, &quot;Helvetica Neue&quot;, Arial, sans-serif; font-kerning: auto; font-size: 20px; font-stretch: 100%; font-style: normal; font-variant: normal; font-weight: 400; height: 50px; image-rendering: auto; isolation: auto; place-items: normal; place-self: auto; left: auto; letter-spacing: normal; line-height: normal; list-style: disc outside none; max-height: none; max-width: 100%; min-height: 0px; min-width: 0px; mix-blend-mode: normal; object-fit: fill; object-position: 50% 50%; offset: none 0px auto 0deg; opacity: 1; orphans: 2; outline: rgb(40, 40, 40) none 0px; outline-offset: 0px; overflow-anchor: auto; overflow-wrap: normal; overflow: visible; padding: 12px 8px; pointer-events: auto; position: static; resize: none; right: auto; scroll-behavior: auto; speak: normal; table-layout: auto; tab-size: 8; text-align: start; text-align-last: auto; text-decoration: none solid rgb(40, 40, 40); text-decoration-skip-ink: auto; text-underline-position: auto; text-indent: 0px; text-rendering: auto; text-shadow: none; text-size-adjust: 100%; text-overflow: clip; text-transform: none; top: auto; touch-action: auto; transition: none 0s ease 0s; unicode-bidi: normal; vertical-align: baseline; visibility: visible; white-space: normal; widows: 2; width: 498px; will-change: auto; word-break: normal; word-spacing: 0px; word-wrap: normal; z-index: auto; zoom: 1; -webkit-appearance: none; backface-visibility: visible; border-spacing: 0px; -webkit-border-image: none; -webkit-box-align: stretch; -webkit-box-decoration-break: slice; -webkit-box-direction: normal; -webkit-box-flex: 0; -webkit-box-flex-group: 1; -webkit-box-lines: single; -webkit-box-ordinal-group: 1; -webkit-box-orient: horizontal; -webkit-box-pack: start; columns: auto auto; column-gap: normal; column-rule: 0px none rgb(40, 40, 40); column-span: none; place-content: normal; flex: 0 1 auto; flex-flow: row nowrap; -webkit-font-smoothing: antialiased; grid-auto-columns: auto; grid-auto-flow: row; grid-auto-rows: auto; grid-area: auto / auto / auto / auto; grid-template-areas: none; grid-template-columns: none; grid-template-rows: none; grid-gap: 0px 0px; -webkit-highlight: none; hyphens: manual; -webkit-hyphenate-character: auto; -webkit-line-break: auto; -webkit-locale: &quot;en&quot;; -webkit-margin-collapse: collapse collapse; -webkit-mask-box-image-source: none; -webkit-mask-box-image-slice: 0 fill; -webkit-mask-box-image-width: auto; -webkit-mask-box-image-outset: 0px; -webkit-mask-box-image-repeat: stretch; -webkit-mask: none 0% 0% / auto repeat border-box border-box; -webkit-mask-composite: source-over; order: 0; perspective: none; perspective-origin: 249.113px 25px; -webkit-print-color-adjust: economy; -webkit-rtl-ordering: logical; shape-outside: none; shape-image-threshold: 0; shape-margin: 0px; -webkit-tap-highlight-color: rgba(0, 0, 0, 0.18); -webkit-text-combine: none; -webkit-text-decorations-in-effect: none; -webkit-text-emphasis: none rgb(40, 40, 40); -webkit-text-emphasis-position: over right; -webkit-text-fill-color: rgb(40, 40, 40); -webkit-text-orientation: vertical-right; -webkit-text-security: none; -webkit-text-stroke: 0px rgb(40, 40, 40); transform: none; transform-origin: 249.113px 25px 0px; transform-style: flat; -webkit-user-drag: auto; -webkit-user-modify: read-only; user-select: auto; -webkit-writing-mode: horizontal-tb; -webkit-app-region: no-drag; buffered-rendering: auto; clip-path: none; clip-rule: nonzero; mask: none; filter: none; flood-color: rgb(0, 0, 0); flood-opacity: 1; lighting-color: rgb(255, 255, 255); stop-color: rgb(0, 0, 0); stop-opacity: 1; color-interpolation: sRGB; color-interpolation-filters: linearRGB; color-rendering: auto; fill: rgb(0, 0, 0); fill-opacity: 1; fill-rule: nonzero; marker: none; mask-type: luminance; shape-rendering: auto; stroke: none; stroke-dasharray: none; stroke-dashoffset: 0px; stroke-linecap: butt; stroke-linejoin: miter; stroke-miterlimit: 4; stroke-opacity: 1; stroke-width: 1px; alignment-baseline: auto; baseline-shift: 0px; dominant-baseline: auto; text-anchor: start; writing-mode: horizontal-tb; vector-effect: none; paint-order: fill; d: none; cx: 0px; cy: 0px; x: 0px; y: 0px; r: 0px; rx: auto; ry: auto; caret-color: rgb(40, 40, 40); line-break: auto;']")]
        private IWebElement _cardNumberInput { get; set; }

        //Expire date - Month
        [FindsBy(How = How.Id, Using = "gatewayCardExpiryDateMonth")]
        private IWebElement _expiryMonthSelect { get; set; }

        //Expire date - Year
        [FindsBy(How = How.Id, Using = "gatewayCardExpiryDateYear")]
        private IWebElement _expiryYearSelect { get; set; }

        //CVC
        [FindsBy(How = How.Id, Using = "/html/body/input")]
        private IWebElement _cvcInput { get; set; }


        //Direct Debit

        //Term
        [FindsBy(How = How.Id, Using = "PayDirectDebitViewModel_SelectedInstallmentOption")]
        private IWebElement _termSelect { get; set; }
        
        //Day of month
        [FindsBy(How = How.Id, Using = "PayDirectDebitViewModel_PayingDayOfMonth")]
        private IWebElement _dayOfMonthInput { get; set; }

        //Holders Name
        [FindsBy(How = How.Id, Using = "PayDirectDebitViewModel_AccountHoldersName")]
        private IWebElement _holdersNameInput{ get; set; }

        //Account number
        [FindsBy(How = How.Id, Using = "PayDirectDebitViewModel_AccountNumber")]
        private IWebElement _accountNumberInput { get; set; }
        
        //Sort code
        [FindsBy(How = How.Id, Using = "PayDirectDebitViewModel_SortCodePart1")]
        private IWebElement _sortCode1Input { get; set; }

        [FindsBy(How = How.Id, Using = "PayDirectDebitViewModel_SortCodePart2")]
        private IWebElement _sortCode2Input { get; set; }

        [FindsBy(How = How.Id, Using = "PayDirectDebitViewModel_SortCodePart3")]
        private IWebElement _sortCode3Input { get; set; }

        //First DD statement
        [FindsBy(How = How.XPath, Using = "//*[@name='TermOne.IsTermAccepted' and @value='True']")]
        private IWebElement _firstDDStatementRadio { get; set; }

        //Second DD statement
        [FindsBy(How = How.XPath, Using = "//*[@name='TermTwo.IsTermAccepted' and @value='True']")]
        private IWebElement _secondDDStatementRadio { get; set; }


        //Correct address - Yes Radio
        [FindsBy(How = How.XPath, Using = "//*[@name='BillingAddress.IsThisCorrectAddress' and @value='True']")]
        private IWebElement _addressConfirmYesRadio { get; set; }

        //Correct address - No Radio
        [FindsBy(How = How.XPath, Using = "//*[@name='BillingAddress.IsThisCorrectAddress' and @value='False']")]
        private IWebElement _addressConfirmNoRadio { get; set; }

        //Notification at the top
        [FindsBy(How = How.XPath, Using = "//*[@class='close-notification']")]
        private IWebElement _closeNotificationButton { get; set; }


        //Continue button
        [FindsBy(How = How.Id, Using = "submit-form")]
        private IWebElement _continueButton { get; set; }

        //Save and complete later button
        [FindsBy(How = How.Id, Using = "ssubmit-sacl-form")]
        private IWebElement _saveAndCompleteLaterButton { get; set; }

        public PrepaidFuneralPlan_PaymentDetails(IWebDriver webDriver)
        {
            Driver = webDriver;
            PageFactory.InitElements(webDriver, this);
        }

        public static PrepaidFuneralPlan_PaymentDetails Navigate(IWebDriver currentWebDriver)
        {
           return new PrepaidFuneralPlan_PaymentDetails(currentWebDriver);
        }

        public void SelectPaymentInFull()
        {
            _fullPaymentRadio.Click();
        }

        public void SelectDirectdebit()
        {
            _directDebitRadio.Click();
        }

        public void EnterPromoCode()
        {
            _promoCodeNoRadio.Click();
            Thread.Sleep(2000);
            _closeNotificationButton.Click();
            Thread.Sleep(2000);
            
        }

        public void SelectNoPromoCode()
        {
            _promoCodeNoRadio.Click();
            Thread.Sleep(2000);
            _closeNotificationButton.Click();
            Thread.Sleep(2000);
        }

        public void SelectCardType()
        {
            var cardSelect = _typeOfCardSelect;
            var selectElementCardType = new SelectElement(cardSelect);
            selectElementCardType.SelectByValue("mastercard");
        }

        public void EnterNameOnCard()
        {
            _nameOnCardInput.SendKeys("testingautomation");    
        }


        public void SelectExpiryDate()
        {
            var monthSelect = _expiryMonthSelect;
            var selectElementMonth = new SelectElement(monthSelect);
            selectElementMonth.SelectByValue("5");

            var yearSelect = _expiryYearSelect;
            var selectElementYear = new SelectElement(yearSelect);
            selectElementYear.SelectByValue("2021");

        }

        public void ConfirmAddress()
        {
            _addressConfirmYesRadio.Click();
        }

        public void EnterCardNumber()
        {
            var _frameCardNumber = Driver.FindElement(By.ClassName("gw-proxy-number"));
            Driver.SwitchTo().Frame(_frameCardNumber);
            Driver.FindElement(By.CssSelector("input")).SendKeys("2223000000000023");
            
        }

        public void EnterCVCCode()
        {
            Driver.SwitchTo().ParentFrame();
            var _frameCVCNumber = Driver.FindElement(By.ClassName("gw-proxy-securityCode"));
            Driver.SwitchTo().Frame(_frameCVCNumber);
            Driver.FindElement(By.CssSelector("input")).SendKeys("222");
        }

        public void SelectTerm()
        {
            var termSelect = _termSelect;
            var selectElementTerm = new SelectElement(termSelect);
            selectElementTerm.SelectByValue("6");
        }

        public void EnterDayOfMonth()
        {
            _dayOfMonthInput.SendKeys("1");
        }

        public void EnterHoldersName()
        {
            _holdersNameInput.SendKeys("testingautomation");
        }

        public void EnterAccountNumber()
        {
            _accountNumberInput.SendKeys("12345677");
        }

        public void EnterSortCode()
        {
            _sortCode1Input.SendKeys("00");
            _sortCode2Input.SendKeys("00");
            _sortCode3Input.SendKeys("04");
        }

        public void AcceptTwoStatements()
        {
           // IJavaScriptExecutor js = Driver as IJavaScriptExecutor;
           // js.ExecuteScript("arguments[0].scrollIntoView();", _firstDDStatementRadio);

           
            _firstDDStatementRadio.Click();
            _secondDDStatementRadio.Click();
        }


        public void ClickOnContinue()
        {
            Driver.SwitchTo().ParentFrame();
            _continueButton.Click();
        }

       
    }
}
