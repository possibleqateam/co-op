﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium;

namespace Co_op.Ecommerce.POP
{
    public class Contact_page
    {
        public readonly IWebDriver Driver;


        [FindsBy(How = How.Id, Using = "account")]
        public IWebElement MyAccount { get; set; }

        [FindsByAll(How = How.TagName, Using = "img")]
        public IWebElement AllImages { get; set; }



    }
}
