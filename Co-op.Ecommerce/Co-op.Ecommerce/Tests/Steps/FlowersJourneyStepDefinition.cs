﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Co_op.Ecommerce.Core;
using Co_op.Ecommerce.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;


namespace Co_op.Ecommerce.Tests.Steps
{
    [Binding]
    public sealed class FlowersJourneyStepDefinition: TestStepBase
    {
        // For additional details on SpecFlow step definitions see http://go.specflow.org/doc-stepdef

        private Flowers_YourDetails _currentFlowers_YourDetailsPage;
        private Flowers_PaymentDetails _currentFlowers_PaymentDetailsPage;
        private Flowers_Summary _currentFlowers_SummaryPage;

        [Given(@"I am on the Co-op main page")]
        public void GivenIAmOnTheCo_OpMainPage()
        {
            _currentFlowers_YourDetailsPage = Flowers_YourDetails.Navigate(Driver);
           
        }

        [Given(@"I am on the CMS side")]
        public void GivenIAmOnTheCMSSide()
        {
            //
        }

        [When(@"I have added flowers to the basket")]
        public void WhenIHaveAddedFlowersToTheBasket()
        {
            _currentFlowers_YourDetailsPage.CheckShoppingBasket();
        }

        [Then(@"I should see flowers in basket")]
        public void ThenIShouldSeeFlowersInBasket()
        {
            Assert.True(Driver.Title.Contains("Your details"));
        }

        [Given(@"I am on the first step of Floral journey")]
        public void GivenIAmOnTheFirstStepOfFloralJourney()
        {
            _currentFlowers_YourDetailsPage = Flowers_YourDetails.Navigate(Driver);
            _currentFlowers_YourDetailsPage.CheckShoppingBasket();
        }

        [Given(@"I have entered all the personal details")]
        public void GivenIHaveEnteredAllThePersonalDetails()
        {
           _currentFlowers_YourDetailsPage.CheckMrRadioButton();
           _currentFlowers_YourDetailsPage.EnterFirstname();
           _currentFlowers_YourDetailsPage.EnterLastname();
           _currentFlowers_YourDetailsPage.EnterDOB();

           _currentFlowers_YourDetailsPage.EnterTelephoneNumber();
        }

        [Given(@"I have provided email address and confim email address")]
        public void GivenIHaveProvidedEmailAddressAndConfimEmailAddress()
        {
            _currentFlowers_YourDetailsPage.EnterEmailAddress();
        }

        [When(@"I have clicked on Continue button")]
        public void WhenIHaveClickedOnContinueButton()
        {
            _currentFlowers_YourDetailsPage.ContinueToTheNextStep();
            Thread.Sleep(5000);

        }

        [Then(@"I should be redirected to Payment details step")]
        public void ThenIShouldBeRedirectedToPaymentDetailsStep()
        {
            IWebElement _titleText = Driver.FindElement(By.TagName("H1"));
            Assert.True(_titleText.Text.Contains("Step 2 - Payment details"));
        }

        [Given(@"I am on the second step of Floral journey")]
        public void GivenIAmOnTheSecondStepOfFloralJourney()
        {
            GivenIAmOnTheFirstStepOfFloralJourney();
            GivenIHaveEnteredAllThePersonalDetails();
            GivenIHaveProvidedEmailAddressAndConfimEmailAddress();
            WhenIHaveClickedOnContinueButton();
          
            _currentFlowers_PaymentDetailsPage = Flowers_PaymentDetails.Navigate(Driver);
        }

        [Given(@"I have entered all card details correctly")]
        public void GivenIHaveEnteredAllCardDetailsCorrectly()
        {
            _currentFlowers_PaymentDetailsPage.SelectCardType();
            _currentFlowers_PaymentDetailsPage.EnterNameOnCard();
            _currentFlowers_PaymentDetailsPage.SelectExpiryDate();

            _currentFlowers_PaymentDetailsPage.EnterCardNumber();
            _currentFlowers_PaymentDetailsPage.EnterCVCCode();
        }

        [Given(@"I have accepted address details and membership")]
        public void GivenIHaveAcceptedAddressDetailsAndMembership()
        {
            _currentFlowers_PaymentDetailsPage.EnterPostcode();
            _currentFlowers_PaymentDetailsPage.SelectMembership();
        }

        [When(@"I have clicked on Continue button at the bottom")]
        public void WhenIHaveClickedOnContinueButtonAtTheBottom()
        {
            _currentFlowers_PaymentDetailsPage.ClickContinue();
            Thread.Sleep(2000);
        }

        [Then(@"I should be redirected to Summary step")]
        public void ThenIShouldBeRedirectedToSummaryStep()
        {
            IWebElement _titleText = Driver.FindElement(By.TagName("H1"));
            Assert.True(_titleText.Text.Contains("Step 3 - Summary"));
        }

        [Given(@"I am on the third step of Floral journey")]
        public void GivenIAmOnTheThirdStepOfFloralJourney()
        {
            GivenIAmOnTheSecondStepOfFloralJourney();
            GivenIHaveEnteredAllCardDetailsCorrectly();
            GivenIHaveAcceptedAddressDetailsAndMembership();
            WhenIHaveClickedOnContinueButtonAtTheBottom();
        }

        [Given(@"I have accepted terms and conditions")]
        public void GivenIHaveAcceptedTermsAndConditions()
        {
            _currentFlowers_SummaryPage = Flowers_Summary.Navigate(Driver);
            _currentFlowers_SummaryPage.AcceptTerms();
        }

        [When(@"I have clicked on Pay button at the bottom")]
        public void WhenIHaveClickedOnPayButtonAtTheBottom()
        {
            _currentFlowers_SummaryPage.ClickPay();
            Thread.Sleep(4000);
        }

        [Then(@"I should be redirected to Thank you step")]
        public void ThenIShouldBeRedirectedToThankYouStep()
        {
            IWebElement title = Driver.FindElement(By.TagName("H1"));
            Assert.True(title.Text.Contains("Purchase Successful"));
        }



    }
}
