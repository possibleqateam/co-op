﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Co_op.Ecommerce.Core;
using Co_op.Ecommerce.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;

namespace Co_op.Ecommerce.Tests.Steps
{
    [Binding]
    public sealed class OnlineMemorialJourneySteps : TestStepBase
    {
        
        private OnlineMemorial_DeliveryDetails _currentOnlineMemorial_DeliveryDetailsPage;
        private OnlineMemorial_PaymentDetails _currentOnlineMemorial_PaymentDetailsPage;
        


        [Given(@"I am on the first step of Online Memorial")]
        public void GivenIAmOnTheFirstStepOfOnlineMemorial()
        {
            _currentOnlineMemorial_DeliveryDetailsPage = OnlineMemorial_DeliveryDetails.Navigate(Driver);

            IWebElement _sendFormBtn = Driver.FindElement(By.XPath("/html/body/form[1]/input[31]"));
            _sendFormBtn.Click();
        }

        [Given(@"I have entered all delivery details")]
        public void GivenIHaveEnteredAllDeliveryDetails()
        {
            _currentOnlineMemorial_DeliveryDetailsPage.CheckMrRadioButton();
            _currentOnlineMemorial_DeliveryDetailsPage.EnterFirstname();
            _currentOnlineMemorial_DeliveryDetailsPage.EnterLastname();
            _currentOnlineMemorial_DeliveryDetailsPage.SelectCountry();
            _currentOnlineMemorial_DeliveryDetailsPage.EnterPostcode();
            Thread.Sleep(2000);
        }

        [When(@"I have clicked Continue button on step (.*)")]
        public void WhenIHaveClickedContinueButtonOnStep(int p0)
        {
            
            _currentOnlineMemorial_DeliveryDetailsPage.ContinueToTheNextStep();
            Thread.Sleep(2000);
        }

        [Then(@"I should be redirected to Step (.*) - Payment details")]
        public void ThenIShouldBeRedirectedToStep_PaymentDetails(int p0)
        {

            //Check title
            IWebElement _titleText = Driver.FindElement(By.TagName("H1"));
            Assert.True(_titleText.Text.Contains("Step 2 - Payment details"));

            IWebElement _step1List = Driver.FindElement(By.XPath("//*[@id='steps']/li[2]/span/span[2]/span[2]"));
            //Assert.True(_step1List.Text.Contains("Payment details")); 
        }

        [Given(@"I am on the second step of Online Memorial")]
        public void GivenIAmOnTheSecondStepOfOnlineMemorial()
        {
            GivenIAmOnTheFirstStepOfOnlineMemorial();
            GivenIHaveEnteredAllDeliveryDetails();
            WhenIHaveClickedContinueButtonOnStep(1);
           _currentOnlineMemorial_PaymentDetailsPage = OnlineMemorial_PaymentDetails.Navigate(Driver);

        }

        [Given(@"I have entered all card details")]
        public void GivenIHaveEnteredAllCardDetails()
        {
            _currentOnlineMemorial_PaymentDetailsPage = OnlineMemorial_PaymentDetails.Navigate(Driver);
            _currentOnlineMemorial_PaymentDetailsPage.SelectCardType();
            _currentOnlineMemorial_PaymentDetailsPage.EnterNameOnCard();

            _currentOnlineMemorial_PaymentDetailsPage.SelectExpiryDate();
            _currentOnlineMemorial_PaymentDetailsPage.EnterCardNumber();
            
            _currentOnlineMemorial_PaymentDetailsPage.EnterCVCCode();


            
            _currentOnlineMemorial_PaymentDetailsPage.SelectBillingAddress();
            _currentOnlineMemorial_PaymentDetailsPage.SelectMembership();
        }

        [Given(@"I have accepted Terms and Conditions")]
        public void GivenIHaveAcceptedTermsAndConditions()
        {
            _currentOnlineMemorial_PaymentDetailsPage.AcceptTerms();
        }

        [When(@"I have clicked Buy button on step (.*)")]
        public void WhenIHaveClickedBuyButtonOnStep(int p0)
        {
            _currentOnlineMemorial_PaymentDetailsPage.ClickOnContinue();
        }

        [Then(@"I should see Summary page for Online Memorial")]
        public void ThenIShouldSeeSummaryPageForOnlineMemorial()
        {
            Thread.Sleep(2000);
            //Check title
            IWebElement _titleText = Driver.FindElement(By.TagName("H1"));
            Assert.True(_titleText.Text.Contains("Your purchase is now complete"));

            
        }

        [Given(@"I am on the Online Memorial Extensions Payment details page")]
        public void GivenIAmOnTheOnlineMemorialExtensionsPaymentDetailsPage()
        {
            _currentOnlineMemorial_DeliveryDetailsPage = OnlineMemorial_DeliveryDetails.Navigate(Driver);
            IWebElement _sendFormBtn = Driver.FindElement(By.XPath("/html/body/form[2]/input[31]"));
            _sendFormBtn.Click();
        }

        [Then(@"I should see Summary page for Online Memorial Extensions")]
        public void ThenIShouldSeeSummaryPageForOnlineMemorialExtensions()
        {
            Thread.Sleep(2000);
            //Check title
            IWebElement _titleText = Driver.FindElement(By.TagName("H1"));
            Assert.True(_titleText.Text.Contains("Your purchase is now complete"));
        }


    }
}
