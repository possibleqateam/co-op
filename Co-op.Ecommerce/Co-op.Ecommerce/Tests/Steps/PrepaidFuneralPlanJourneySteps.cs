﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using Co_op.Ecommerce.Core;
using Co_op.Ecommerce.POM;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Threading;


namespace Co_op.Ecommerce.Tests.Steps
{
    [Binding]
    public sealed class PrepaidFuneralPlanJourneySteps : TestStepBase
    {
        private PrepaidFuneralPlan_Start _currentPrepaidFuneralPlan_StartPage;
        private PrepaidFuneralPlan_PlanHolderDetails _currentPrepaidFuneralPlan_PlanHolderDetailsPage;
        private PrepaidFuneralPlan_YourDetails _currentPrepaidFuneralPlan_YourDetailsPage;
        private PrepaidFuneralPlan_PaymentDetails _currentPrepaidFuneralPlan_PaymentDetailsPage;
        private PrepaidFuneralPlan_ThankYou _currentPrepaidFuneralPlan_ThankYouPage;
        private PrepaidFuneralPlan_Calculator _currentPrepaidFuneralPlan_CalculatorPage;


        [Given(@"I am on the Funeral Plan Calculator Page")]
        public void GivenIAmOnTheFuneralPlanCalculatorPage()
        {

            _currentPrepaidFuneralPlan_CalculatorPage = PrepaidFuneralPlan_Calculator.Navigate(Driver);
        }
        
        [Given(@"I have chosen Burial type of funeral")]
        public void GivenIHaveChosenBurialTypeOfFuneral()
        {
            _currentPrepaidFuneralPlan_CalculatorPage.SelectTypeOfFuneralBurial();
        }
        
        [Given(@"I have entered all valid details")]
        public void GivenIHaveEnteredAllValidDetails()
        {
            _currentPrepaidFuneralPlan_CalculatorPage.SelectFuneralPlanSimple();
            _currentPrepaidFuneralPlan_CalculatorPage.BuyFuneralPlanOnline();
            _currentPrepaidFuneralPlan_CalculatorPage.PayInFull();
            _currentPrepaidFuneralPlan_CalculatorPage.NoCoopMember();
            _currentPrepaidFuneralPlan_CalculatorPage.PromoCode();
        }
        
        [Given(@"I have chosen Cremation type of funeral")]
        public void GivenIHaveChosenCremationTypeOfFuneral()
        {
            _currentPrepaidFuneralPlan_CalculatorPage.SelectTypeOfFuneralCremation();
        }
        
        [Given(@"I am on the step zero for Funeral Plans journey")]
        public void GivenIAmOnTheStepZeroForFuneralPlansJourney()
        {
            _currentPrepaidFuneralPlan_StartPage = PrepaidFuneralPlan_Start.Navigate(Driver);
           
        }
        
        [Given(@"I can choose Cremation option")]
        public void GivenICanChooseCremationOption()
        {
            _currentPrepaidFuneralPlan_StartPage.CheckCremationRadioIfExist();
        }
        
        [Given(@"I can choose Burial option")]
        public void GivenICanChooseBurialOption()
        {
            _currentPrepaidFuneralPlan_StartPage.CheckBurialRadioIfExist();
        }
        
        [Given(@"I have chosen Cremation option")]
        public void GivenIHaveChosenCremationOption()
        {
            _currentPrepaidFuneralPlan_StartPage.SelectCremationRadio();
        }
        
        [Given(@"I have chosen Level of plan")]
        public void GivenIHaveChosenLevelOfPlan()
        {
            _currentPrepaidFuneralPlan_StartPage.SelectSimplePlanRadio();
        }
        
        [Given(@"I have chosen Burial option")]
        public void GivenIHaveChosenBurialOption()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"I was on step zero and I have clicked Continue")]
        public void GivenIWasOnStepZeroAndIHaveClickedContinue()
        {
            _currentPrepaidFuneralPlan_StartPage = PrepaidFuneralPlan_Start.Navigate(Driver);
            _currentPrepaidFuneralPlan_StartPage.SelectCremationRadio();
            _currentPrepaidFuneralPlan_StartPage.SelectSimplePlanRadio();
            _currentPrepaidFuneralPlan_StartPage.ClickImReadytoContinue();

        }
        
        [Given(@"I can see Create account and Login buttons on the next substep")]
        public void GivenICanSeeCreateAccountAndLoginButtonsOnTheNextSubstep()
        {
            //Check if Create account and Login button are on the pre-step page
            IWebElement _createAccountBtn = Driver.FindElement(By.LinkText("Create account"));
            Assert.True(_createAccountBtn.Displayed);

            IWebElement _logintBtn = Driver.FindElement(By.Id("submit"));
            Assert.True(_logintBtn.Displayed);

            IWebElement _emailInput = Driver.FindElement(By.Id("Email"));
            Assert.True(_emailInput.Displayed);

            IWebElement _passwordInput = Driver.FindElement(By.Id("Password"));
            Assert.True(_passwordInput.Displayed);
        }
        
        [Given(@"I am on the first step of Funeral Plan for unauthenticated user")]
        public void GivenIAmOnTheFirstStepOfFuneralPlanForUnauthenticatedUser()
        {
            GivenIWasOnStepZeroAndIHaveClickedContinue();
            WhenIHaveClickedOnCreateAccountButton();

        }
        
        [Given(@"I have entered all personal details correctly")]
        public void GivenIHaveEnteredAllPersonalDetailsCorrectly()
        {
            _currentPrepaidFuneralPlan_YourDetailsPage = PrepaidFuneralPlan_YourDetails.Navigate(Driver);


            _currentPrepaidFuneralPlan_YourDetailsPage.CheckMrRadioButton();
            _currentPrepaidFuneralPlan_YourDetailsPage.EnterFirstname();
            _currentPrepaidFuneralPlan_YourDetailsPage.EnterLastname();
            _currentPrepaidFuneralPlan_YourDetailsPage.EnterDOB();
            _currentPrepaidFuneralPlan_YourDetailsPage.EnterPostcode();
            _currentPrepaidFuneralPlan_YourDetailsPage.EnterTelephoneNumber();
            _currentPrepaidFuneralPlan_YourDetailsPage.SelectMembership();
            _currentPrepaidFuneralPlan_YourDetailsPage.EnterPassword();
            _currentPrepaidFuneralPlan_YourDetailsPage.ConfirmPassword();

        }
        
        [Given(@"I have provided unique email address")]
        public void GivenIHaveProvidedUniqueEmailAddress()
        {
            _currentPrepaidFuneralPlan_YourDetailsPage.EnterEmailAddress();
        }
        
        [Given(@"I am on the first step of Funeral Plan for already authenticated user")]
        public void GivenIAmOnTheFirstStepOfFuneralPlanForAlreadyAuthenticatedUser()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"I all the personal details were filled out already")]
        public void GivenIAllThePersonalDetailsWereFilledOutAlready()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"I have provided all details and clicked on Buy now button")]
        public void GivenIHaveProvidedAllDetailsAndClickedOnBuyNowButton()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"I went thtought all the steps of Funeral Plan")]
        public void GivenIWentThtoughtAllTheStepsOfFuneralPlan()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I press Calculate button")]
        public void WhenIPressCalculateButton()
        {
            _currentPrepaidFuneralPlan_CalculatorPage.ClickOnCalculate();
        }
        
        [When(@"After clicking on Buy now button")]
        public void WhenAfterClickingOnBuyNowButton()
        {
            _currentPrepaidFuneralPlan_CalculatorPage.ClickOnPay();
        }
        
        [When(@"I click on Continue button but nothing was selected")]
        public void WhenIClickOnContinueButtonButNothingWasSelected()
        {
            _currentPrepaidFuneralPlan_StartPage.ClickImReadytoContinue();
        }
        
        [When(@"I click on Continue button")]
        public void WhenIClickOnContinueButton()
        {
            _currentPrepaidFuneralPlan_StartPage.ClickImReadytoContinue();
        }
        
        [When(@"I have clicked on Create account button")]
        public void WhenIHaveClickedOnCreateAccountButton()
        {
            //Click on Create account button
            IWebElement _createAccountBtn = Driver.FindElement(By.LinkText("Create account"));
            _createAccountBtn.Click();

        }
        
        [When(@"I have entered valid email and password button")]
        public void WhenIHaveEnteredValidEmailAndPasswordButton()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I have clicked on Login button")]
        public void WhenIHaveClickedOnLoginButton()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"I have clicked on Continue button on step (.*)")]
        public void WhenIHaveClickedOnContinueButtonOnStep(int p0)
        {
            _currentPrepaidFuneralPlan_YourDetailsPage.ContinueToTheNextStep();
        }
        
        [When(@"I have paid for the plan")]
        public void WhenIHavePaidForThePlan()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"I should see sliders with Low Cost Monthly Instalment Plan based on the criteria from previous steps")]
        public void ThenIShouldSeeSlidersWithLowCostMonthlyInstalmentPlanBasedOnTheCriteriaFromPreviousSteps()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"I should be redirected to the step zero of Funeral Plan")]
        public void ThenIShouldBeRedirectedToTheStepZeroOfFuneralPlan()
        {
            Assert.True(Driver.Url.Contains("pre-paid-funeral-plans"));
        }
        
        [Then(@"I should see validation and error messages")]
        public void ThenIShouldSeeValidationAndErrorMessages()
        {
            _currentPrepaidFuneralPlan_StartPage.CheckErrorMessage();
        }
        
        [Then(@"I should be redirected to the next step")]
        public void ThenIShouldBeRedirectedToTheNextStep()
        {
            //Check if Create account and Login button are on the pre-step page
            IWebElement _createAccountBtn = Driver.FindElement(By.LinkText("Create account"));
            Assert.True(_createAccountBtn.Displayed);

            IWebElement _logintBtn = Driver.FindElement(By.Id("submit"));
            Assert.True(_logintBtn.Displayed);

            IWebElement _emailInput = Driver.FindElement(By.Id("Email"));
            Assert.True(_emailInput.Displayed);

            IWebElement _passwordInput = Driver.FindElement(By.Id("Password"));
            Assert.True(_passwordInput.Displayed);

        }
        
        [Then(@"I should be redirected to the first step of Funeral Plan journey for new user")]
        public void ThenIShouldBeRedirectedToTheFirstStepOfFuneralPlanJourneyForNewUser()
        {
            IWebElement _titleText = Driver.FindElement(By.TagName("H1"));
            Assert.True(_titleText.Text.Contains("Your Details"));

            IWebElement _step1List = Driver.FindElement(By.XPath("//*[@id='steps']/li[1]/span/span[2]/span[2]"));
            Assert.True(_step1List.Text.Contains("Your Details")); 
        }
        
        [Then(@"I should be redirected to the first step of Funeral Plan journey with logged in user")]
        public void ThenIShouldBeRedirectedToTheFirstStepOfFuneralPlanJourneyWithLoggedInUser()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"I should be redirected to step (.*) - Plan Holder Details")]
        public void ThenIShouldBeRedirectedToStep_PlanHolderDetails(int p0)
        {
           //Check title
            IWebElement _titleText = Driver.FindElement(By.TagName("H1"));
            Assert.True(_titleText.Text.Contains("Step 2 - Plan Holder Details"));

            IWebElement _step1List = Driver.FindElement(By.XPath("//*[@id='steps']/li[2]/span/span[2]/span[2]"));
            Assert.True(_step1List.Text.Contains("Plan Holder Details")); 
        }
        
        [Then(@"It should be visible in the basket")]
        public void ThenItShouldBeVisibleInTheBasket()
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"I am on the second step of Funeral Plan")]
        public void GivenIAmOnTheSecondStepOfFuneralPlan()
        {
            GivenIWasOnStepZeroAndIHaveClickedContinue();
            WhenIHaveClickedOnCreateAccountButton();
            GivenIHaveEnteredAllPersonalDetailsCorrectly();
            GivenIHaveProvidedUniqueEmailAddress();
            WhenIHaveClickedOnContinueButtonOnStep(1);

            
            _currentPrepaidFuneralPlan_PlanHolderDetailsPage = PrepaidFuneralPlan_PlanHolderDetails.Navigate(Driver);
           
            
        }

        [Given(@"I have chosen that this Funeral Plan is for me")]
        public void GivenIHaveChosenThatThisFuneralPlanIsForMe()
        {
            _currentPrepaidFuneralPlan_PlanHolderDetailsPage.SelectPlanForMe();
        }

        [When(@"I clicked on Continue button on step (.*)")]
        public void WhenIClickedOnContinueButtonOnStep(int p0)
        {
            _currentPrepaidFuneralPlan_PlanHolderDetailsPage.ClickOnContinue();
        }

        [Then(@"I should be redirected to step (.*) - Payment Details")]
        public void ThenIShouldBeRedirectedToStep_PaymentDetails(int p0)
        {
            //Check title
            IWebElement _titleText = Driver.FindElement(By.TagName("H1"));
            Assert.True(_titleText.Text.Contains("Step 3 - Payment Details"));

            IWebElement _step1List = Driver.FindElement(By.XPath("//*[@id='steps']/li[3]/span/span[2]/span[2]"));
            Assert.True(_step1List.Text.Contains("Payment Details")); 
        }

        [Given(@"I have chosen that this Funeral Plan is for someone else")]
        public void GivenIHaveChosenThatThisFuneralPlanIsForSomeoneElse()
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"I have entered the details of the person I am buying the Plan for")]
        public void GivenIHaveEnteredTheDetailsOfThePersonIAmBuyingThePlanFor()
        {
            ScenarioContext.Current.Pending();
        }

        [Given(@"I am on the third step of Funeral Plan")]
        public void GivenIAmOnTheThirdStepOfFuneralPlan()
        {
            GivenIWasOnStepZeroAndIHaveClickedContinue();
            WhenIHaveClickedOnCreateAccountButton();
            GivenIHaveEnteredAllPersonalDetailsCorrectly();
            GivenIHaveProvidedUniqueEmailAddress();
            WhenIHaveClickedOnContinueButtonOnStep(1);


            _currentPrepaidFuneralPlan_PlanHolderDetailsPage = PrepaidFuneralPlan_PlanHolderDetails.Navigate(Driver);
            GivenIHaveChosenThatThisFuneralPlanIsForMe();
            _currentPrepaidFuneralPlan_PlanHolderDetailsPage.ClickOnContinue();

            _currentPrepaidFuneralPlan_PaymentDetailsPage = PrepaidFuneralPlan_PaymentDetails.Navigate(Driver);
        }

        [Given(@"I have chosen One off payment in full")]
        public void GivenIHaveChosenOneOffPaymentInFull()
        {
            _currentPrepaidFuneralPlan_PaymentDetailsPage.SelectPaymentInFull();
        }

        [Given(@"I provided all the details about the credit card")]
        public void GivenIProvidedAllTheDetailsAboutTheCreditCard()
        {
            _currentPrepaidFuneralPlan_PaymentDetailsPage.EnterPromoCode();

            _currentPrepaidFuneralPlan_PaymentDetailsPage.SelectCardType();
            _currentPrepaidFuneralPlan_PaymentDetailsPage.EnterNameOnCard();
         

            _currentPrepaidFuneralPlan_PaymentDetailsPage.SelectExpiryDate();
            _currentPrepaidFuneralPlan_PaymentDetailsPage.ConfirmAddress();

            _currentPrepaidFuneralPlan_PaymentDetailsPage.EnterCardNumber();
            _currentPrepaidFuneralPlan_PaymentDetailsPage.EnterCVCCode();
        }

        [Given(@"I have provided promotional code")]
        public void GivenIHaveProvidedPromotionalCode()
        {
            // _currentPrepaidFuneralPlan_PaymentDetailsPage.EnterPromoCode();
        }

        [When(@"I confirmed credit card details and clicked on Continue button")]
        public void WhenIConfirmedCreditCardDetailsAndClickedOnContinueButton()
        {
            _currentPrepaidFuneralPlan_PaymentDetailsPage.ClickOnContinue();
        }

        [Then(@"I should be redirected to the last step - Summary")]
        public void ThenIShouldBeRedirectedToTheLastStep_Summary()
        {
            Thread.Sleep(10000);
            IWebElement _titleText = Driver.FindElement(By.TagName("H1"));
            Assert.True(_titleText.Text.Contains("Step 4 - Review your Plan"));

            IWebElement _PayNowButton = Driver.FindElement(By.Id("submit-form"));
            Assert.True(_PayNowButton.Text.Contains("Pay now"));
        }

        [Given(@"I have chosen Direct Debit payment")]
        public void GivenIHaveChosenDirectDebitPayment()
        {
            _currentPrepaidFuneralPlan_PaymentDetailsPage.SelectDirectdebit();
            _currentPrepaidFuneralPlan_PaymentDetailsPage.SelectNoPromoCode();
        }

        [Given(@"I have entered all the details related to Direct Debit")]
        public void GivenIHaveEnteredAllTheDetailsRelatedToDirectDebit()
        {
            _currentPrepaidFuneralPlan_PaymentDetailsPage.SelectTerm();
            _currentPrepaidFuneralPlan_PaymentDetailsPage.EnterDayOfMonth();
            _currentPrepaidFuneralPlan_PaymentDetailsPage.EnterHoldersName();
            _currentPrepaidFuneralPlan_PaymentDetailsPage.EnterAccountNumber();
            _currentPrepaidFuneralPlan_PaymentDetailsPage.EnterSortCode();
            _currentPrepaidFuneralPlan_PaymentDetailsPage.ConfirmAddress();
        }

        [Given(@"I have accepted two statements of Direct Debit")]
        public void GivenIHaveAcceptedTwoStatementsOfDirectDebit()
        {
            _currentPrepaidFuneralPlan_PaymentDetailsPage.AcceptTwoStatements();
        }

        [When(@"I confirmed Direct Debit details and clicked on Continue button")]
        public void WhenIConfirmedDirectDebitDetailsAndClickedOnContinueButton()
        {
            _currentPrepaidFuneralPlan_PaymentDetailsPage.ClickOnContinue();
        }

        [Given(@"I am on the last step of Funeral Plan for Direct Debit")]
        public void GivenIAmOnTheLastStepOfFuneralPlanForDirectDebit()
        {
            GivenIAmOnTheThirdStepOfFuneralPlan();

            GivenIHaveChosenDirectDebitPayment();
            GivenIHaveEnteredAllTheDetailsRelatedToDirectDebit();
            GivenIHaveAcceptedTwoStatementsOfDirectDebit();
            WhenIConfirmedDirectDebitDetailsAndClickedOnContinueButton();
        }

        [Given(@"I am on the last step of Funeral Plan for one off payment")]
        public void GivenIAmOnTheLastStepOfFuneralPlanForOneOffPayment()
        {
            GivenIAmOnTheThirdStepOfFuneralPlan();

            GivenIHaveChosenOneOffPaymentInFull();
            GivenIProvidedAllTheDetailsAboutTheCreditCard();
            WhenIConfirmedCreditCardDetailsAndClickedOnContinueButton();
        }


        [Given(@"I am on the last step of Funeral Plan")]
        public void GivenIAmOnTheLastStepOfFuneralPlan()
        {
          
        }

        [Given(@"I have accepted Terms checkbox")]
        public void GivenIHaveAcceptedTermsCheckbox()
        {
            Thread.Sleep(4000);
            _currentPrepaidFuneralPlan_ThankYouPage = PrepaidFuneralPlan_ThankYou.Navigate(Driver);
            _currentPrepaidFuneralPlan_ThankYouPage.Iagree();
        }

        [When(@"I have clicked on Pay now")]
        public void WhenIHaveClickedOnPayNow()
        {
            _currentPrepaidFuneralPlan_ThankYouPage.ClickOnContinue();
        }

        [Then(@"I should finish whole journey for Direct Debit")]
        public void ThenIShouldFinishWholeJourneyForDirectDebit()
        {
            //Check title
            IWebElement _titleText = Driver.FindElement(By.TagName("H1"));
            Assert.True(_titleText.Text.Contains("Purchase Successful"));
        }

        [Then(@"I should finish whole journey for one-off payment")]
        public void ThenIShouldFinishWholeJourneyForOne_OffPayment()
        {
            //Check title
            IWebElement _titleText = Driver.FindElement(By.TagName("H1"));
            Assert.True(_titleText.Text.Contains("Purchase Successful"));
        }


    }
}
