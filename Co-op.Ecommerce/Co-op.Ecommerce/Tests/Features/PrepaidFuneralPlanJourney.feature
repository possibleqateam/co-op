﻿Feature: PrepaidFuneralPlanJourney
	Fill in Funeral Plan Calculator for Burial funeral type
	Fill in Funeral Plan Calculator for Cremation funeral type
	Redirect from Funeral Plan Calculator to the step zero of Funeral Plan for Burial
	Redirect from Funeral Plan Calculator to the step zero of Funeral Plan for Cremation
	First step for Funeral Plans validation
	Choosing Cremation on the step zero of Funeral Plan journey
	Choosing Burial on the step zero of Funeral Plan journey
	Creating account after step zero of Funeral Plan journey
	Logging in after step zero of Funeral Plan journey 
	Step 1 Your details - personal details for unauthenticated user
	Step 1 Your details - personal details for already authenticated user
	Step 2 Plan Holder Details - this Set Funeral Plan is for me
	Step 2 Plan Holder Details - this Set Funeral Plan is for someone else
	Step 3 Payment Details - one-off payment in full
	Step 3 Payment Details - Direct Debit
	Journey throught all the steps - Direct Debit
	Journey throught all the steps - one-off payment in full
	Journey from Funeral Plan Calculator till last step of Pre Paid Funeral Plan

@mytag
Scenario: Fill in Funeral Plan Calculator for Burial funeral type
	Given I am on the Funeral Plan Calculator Page
	And  I have chosen Burial type of funeral
	And I have entered all valid details
	When I press Calculate button
	Then I should see sliders with Low Cost Monthly Instalment Plan based on the criteria from previous steps

Scenario: Fill in Funeral Plan Calculator for Cremation funeral type
	Given I am on the Funeral Plan Calculator Page
	And  I have chosen Cremation type of funeral
	And I have entered all valid details
	When I press Calculate button
	Then I should see sliders with Low Cost Monthly Instalment Plan based on the criteria from previous steps

Scenario: Redirect from Funeral Plan Calculator to the step zero of Funeral Plan for Burial
	Given I am on the Funeral Plan Calculator Page
	And  I have chosen Burial type of funeral
	And I have entered all valid details
	When I press Calculate button
	And After clicking on Buy now button
	Then I should be redirected to the step zero of Funeral Plan 

Scenario: Redirect from Funeral Plan Calculator to the step zero of Funeral Plan for Cremation
	Given I am on the Funeral Plan Calculator Page
	And  I have chosen Cremation type of funeral
	And I have entered all valid details
	When I press Calculate button
	And After clicking on Buy now button
	Then I should be redirected to the step zero of Funeral Plan

Scenario: First step for Funeral Plans validation 
	Given I am on the step zero for Funeral Plans journey
	And I can choose Cremation option
	And I can choose Burial option
	When I click on Continue button but nothing was selected
	Then I should see validation and error messages 

Scenario: Choosing Cremation on the step zero of Funeral Plan journey
	Given I am on the step zero for Funeral Plans journey
	And I have chosen Cremation option
	And I have chosen Level of plan
	When I click on Continue button
	Then I should be redirected to the next step

Scenario: Choosing Burial on the step zero of Funeral Plan journey
	Given I am on the step zero for Funeral Plans journey
	And I have chosen Burial option
	And I have chosen Level of plan
	When I click on Continue button
	Then I should be redirected to the next step

Scenario: Creating account after step zero of Funeral Plan journey
    Given I was on step zero and I have clicked Continue
	And I can see Create account and Login buttons on the next substep
	When I have clicked on Create account button
	Then I should be redirected to the first step of Funeral Plan journey for new user

Scenario: Logging in after step zero of Funeral Plan journey
	Given I was on step zero and I have clicked Continue
	And I can see Create account and Login buttons on the next substep
	When I have entered valid email and password button
	And I have clicked on Login button
	Then I should be redirected to the first step of Funeral Plan journey with logged in user

Scenario: Step 1 Your details - personal details for unauthenticated user
	Given I am on the first step of Funeral Plan for unauthenticated user
	And I have entered all personal details correctly
	And I have provided unique email address
	When I have clicked on Continue button on step 1
	Then I should be redirected to step 2 - Plan Holder Details

Scenario: Step 1 Your details - personal details for already authenticated user
	Given I am on the first step of Funeral Plan for already authenticated user
	And I all the personal details were filled out already
	When I have clicked on Continue button on step 1
	Then I should be redirected to step 2 - Plan Holder Details

Scenario: Step 2 Plan Holder Details - this Set Funeral Plan is for me
	Given I am on the second step of Funeral Plan 
	And I have chosen that this Funeral Plan is for me
	When I clicked on Continue button on step 2
	Then I should be redirected to step 3 - Payment Details

Scenario: Step 2 Plan Holder Details - this Set Funeral Plan is for someone else
	Given I am on the second step of Funeral Plan 
	And I have chosen that this Funeral Plan is for someone else
	And I have entered the details of the person I am buying the Plan for
	When I clicked on Continue button on step 2
	Then I should be redirected to step 3 - Payment Details

Scenario: Step 3 Payment Details - one-off payment in full
	Given I am on the third step of Funeral Plan 
	And I have chosen One off payment in full
	And I provided all the details about the credit card
	And I have provided promotional code
	When I confirmed credit card details and clicked on Continue button
	Then I should be redirected to the last step - Summary 

Scenario: Step 3 Payment Details - Direct Debit
	Given I am on the third step of Funeral Plan 
	And I have chosen Direct Debit payment
	And I have provided promotional code
	And I have entered all the details related to Direct Debit
	And I have accepted two statements of Direct Debit 
	When I confirmed Direct Debit details and clicked on Continue button
	Then I should be redirected to the last step - Summary 

Scenario: Journey throught all the steps - Direct Debit
	Given I am on the last step of Funeral Plan for Direct Debit
	And I have accepted Terms checkbox
	When I have clicked on Pay now
	Then I should finish whole journey for Direct Debit

Scenario: Journey throught all the steps - one-off payment in full
	Given I am on the last step of Funeral Plan for one off payment
	And I have accepted Terms checkbox
	When I have clicked on Pay now
	Then I should finish whole journey for one-off payment 

Scenario: Journey from Funeral Plan Calculator till last step of Pre Paid Funeral Plan
	Given I am on the Funeral Plan Calculator Page
	And I have provided all details and clicked on Buy now button
	And I went thtought all the steps of Funeral Plan
	When I have paid for the plan
	Then It should be visible in the basket