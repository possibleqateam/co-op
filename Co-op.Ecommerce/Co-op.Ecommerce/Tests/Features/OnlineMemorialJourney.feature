﻿Feature: OnlineMemorialJourney
	Online Memorial Book Step 1 - Delivery details
	Online Memorial Book Step 2 - Payment details
	Online Memorial Extensions - Payment details


@mytag
Scenario: Online Memorial Book Step 1 - Delivery details
	Given I am on the first step of Online Memorial
	And I have entered all delivery details 
	When I have clicked Continue button on step 1
	Then I should be redirected to Step 2 - Payment details


Scenario: Online Memorial Book Step 2 - Payment details
    Given I am on the second step of Online Memorial
	And I have entered all card details
	And I have accepted Terms and Conditions
	When I have clicked Continue button on step 2
	Then I should see Summary page for Online Memorial

Scenario: Online Memorial Extensions - Payment details
	Given I am on the Online Memorial Extensions Payment details page
	And I have entered all card details
	And I have accepted Terms and Conditions
	When I have clicked Continue button on step 1 
	Then I should see Summary page for Online Memorial Extensions 

