﻿Feature: FlowersJourney
	Step 1 Floral journey - personal details 
	Step 2 Floral journey - payment details 
	Step 3 Floral journey - summary

@mytag
Scenario: Go to first step
	Given I am on the Co-op main page
	And I am on the CMS side
	When I have added flowers to the basket 
	Then I should see flowers in basket

Scenario: Step 1 Floral journey - personal details 
	Given I am on the first step of Floral journey
	And I have entered all the personal details 
	And I have provided email address and confim email address
    When I have clicked on Continue button
	Then I should be redirected to Payment details step

Scenario: Step 2 Floral journey - payment details 
	Given I am on the second step of Floral journey
	And I have entered all card details correctly
	And I have accepted address details and membership
    When I have clicked on Continue button at the bottom
	Then I should be redirected to Summary step

Scenario: Step 3 Floral journey - summary 
	Given I am on the third step of Floral journey
	And I have accepted terms and conditions
    When I have clicked on Pay button at the bottom
	Then I should be redirected to Thank you step